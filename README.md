Install:

    $ npm install

Automated checks:

    $ npm test

To see for your own eyes:

    $ npm start

And open http://localhost:8080

## TODO

1. [x] code coverage
1. [x] Gitlab CI
1. [x] Google Maps
1. [ ] Refactor client: create declarative Map component
1. [ ] Deploy
