function markersFactory(G) {
  const markers = {};

  return {
    add(id, marker) {
      markers[id] = marker;
    },
    animate(id) {
      if (markers[id]) {
        markers[id].setAnimation(G.maps.Animation.BOUNCE);
      }
    },
    unanimate(id) {
      if (markers[id]) {
        markers[id].setAnimation(null);
      }
    }
  };
}

module.exports = markersFactory;
