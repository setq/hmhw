function google(G) {
  const geocoder = new G.maps.Geocoder();

  return {
    newLatLng(lat, lng) {
      return new G.maps.LatLng(lat, lng);
    },
    newMap(el, options) {
      return new G.maps.Map(el, options);
    },
    newCircle(options) {
      return new G.maps.Circle(options);
    },
    newMarker(options) {
      return new G.maps.Marker(options);
    },
    geocode(request, callback) {
      return geocoder.geocode(request, callback);
    },
    getDistance(from, to) {
      return G.maps.geometry.spherical.computeDistanceBetween(from, to);
    }
  };
}

module.exports = google;
