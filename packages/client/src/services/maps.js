const formatAddress = require('../utils/format-address');

const RADIUS = 20000;

function maps(google, markers) {
  let map;

  const CENTER = google.newLatLng(51.5073835, -0.1277801);

  return {
    CENTER,
    getStatus(property) {
      return new Promise((resolve) => {
        const request = {
          region: 'gb',
          address: formatAddress(property.address)
        };
        google.geocode(request, results => {
          if (results && results.length) {
            const location = results[0].geometry.location;
            const isInsideRadius = google.getDistance(CENTER, location) <= RADIUS;
            markers.add(property.airbnbId, google.newMarker({
              map,
              position: location,
              opacity: isInsideRadius ? 1 : 0.5
            }));
            resolve(isInsideRadius ? 'managed' : 'outside');
          }
          resolve('unknown');
        });
      });
    },
    setMapElement(el) {
      map = google.newMap(el, {
        center: CENTER,
        zoom: 10
      });
      google.newCircle({
        strokeColor: 'tomato',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: 'salmon',
        fillOpacity: 0.35,
        map,
        center: CENTER,
        radius: 20000
      });
    }
  };
}

module.exports = maps;
