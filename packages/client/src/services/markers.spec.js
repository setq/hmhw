const markersFactory = require('./markers');

describe('markers service', () => {
  let markers;

  beforeEach(() => {
    markers = markersFactory(GOOGLE_MOCK);
  });

  it('should animate marker by id', () => {
    const m = sinon.stub(marker());
    markers.add(1, m);
    markers.add(2, m);
    markers.animate(1);
    expect(m.setAnimation).to.be.calledWith(
      GOOGLE_MOCK.maps.Animation.BOUNCE
    );
  });

  it('should stop animation by id', () => {
    const m = sinon.stub(marker());
    markers.add(1, m);
    markers.add(2, marker());
    markers.unanimate(1);
    expect(m.setAnimation).to.be.calledWith(null);
  });

  it('should not fail for unknown id', () => {
    markers.animate(1);
    markers.unanimate(1);
  });

  function marker() {
    return {
      setAnimation() {}
    };
  }
});
