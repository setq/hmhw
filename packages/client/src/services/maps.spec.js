const markersFactory = require('./markers');
const mapsFactory = require('./maps');
const googleFactory = require('./google');

describe('maps service', () => {
  let markers;
  let maps;
  let google;
  let el;
  let map;

  beforeEach(() => {
    el = Math.random();
    map = Math.random();
    google = sinon.stub(googleFactory(GOOGLE_MOCK));
    google.newMap.returns(map);
    google.newLatLng.callsFake((lat, lng) => `LatLng(${lat}, ${lng})`);
    markers = sinon.stub(markersFactory());
    maps = mapsFactory(google, markers);
  });

  it('should expose .CENTER property as LatLng object', () => {
    expect(maps.CENTER).to.equal('LatLng(51.5073835, -0.1277801)');
  });

  describe('setMapElement()', () => {
    beforeEach(() => {
      maps.setMapElement(el);
    });

    it('should create google map', () => {
      expect(google.newMap).to.be.calledWith(el, {
        center: maps.CENTER,
        zoom: 10
      });
    });

    it('should define serviceable area as circle', () => {
      expect(google.newCircle).to.be.calledWith({
        strokeColor: 'tomato',
        strokeOpacity: 0.8,
        strokeWeight: 1,
        fillColor: 'salmon',
        fillOpacity: 0.35,
        map,
        center: maps.CENTER,
        radius: 20000
      });
    });
  });

  describe('getStatus()', () => {
    let property;
    let status;

    beforeEach(() => {
      maps.setMapElement(el);
      property = {
        airbnbId: Math.random(),
        address: {
          line1: 'A',
          line2: 'B',
          line3: 'C',
          line4: 'D',
          postCode: 'E',
          city: 'F',
          country: 'G'
        }
      };
      status = maps.getStatus(property);
    });

    it('should geocode given address', () => {
      expect(google.geocode).to.be.calledWith(
        {
          address: 'A, B, C, D, E, F, G',
          region: 'gb'
        },
        sinon.match.func
      );
    });

    describe('when there are results', () => {
      let results;
      let marker;

      beforeEach(() => {
        results = [aResult(), aResult()];
        marker = Math.random();
        google.newMarker.returns(marker);
        invokeCallback(results);
      });

      it('should create marker for first identified location', () => {
        expect(google.newMarker).to.be.calledWith(sinon.match({
          map,
          position: results[0].geometry.location
        }));
        expect(google.newMarker).not.to.be.calledWith(sinon.match({
          map,
          position: results[1].geometry.location
        }));
      });

      it('should compare distance between center and result location', () => {
        expect(google.getDistance.callCount).to.equal(1);
        expect(google.getDistance).to.be.calledWith(
          maps.CENTER,
          results[0].geometry.location
        );
      });

      it('should register marker to markers service', () => {
        expect(markers.add).to.be.calledWith(property.airbnbId, marker);
      });
    });

    [[null], [undefined], [[], 'empty array'], [{}, 'empty object']]
      .forEach(([result, label]) => {
        describe(`when result is ${label || result}`, () => {
          beforeEach(() => invokeCallback(result));

          it('should not create marker', () => {
            expect(google.newMarker).not.to.be.called();
          });

          it('should return status "unknown"', async () => {
            expect(await status).to.equal('unknown');
          });
        });
      });

    describe('when distance is <= 20km', () => {
      beforeEach(() => {
        google.getDistance.returns(20000);
        invokeCallback([aResult()]);
      });

      it('should set marker opacity to full', () => {
        expect(google.newMarker).to.be.calledWith(sinon.match({opacity: 1}));
      });

      it('should return "managed" as status', async () => {
        expect(await status).to.equal('managed');
      });
    });

    describe('when distance is > 20km', () => {
      beforeEach(() => {
        google.getDistance.returns(20100);
        invokeCallback([aResult()]);
      });

      it('should set marker opacity to half', () => {
        expect(google.newMarker).to.be.calledWith(sinon.match({opacity: 0.5}));
      });

      it('should return "outside" as status', async () => {
        expect(await status).to.equal('outside');
      });
    });

    function invokeCallback() {
      return google.geocode.lastCall.args[1].apply(this, arguments);
    }

    function aResult() {
      return {geometry: {location: Math.random()}};
    }
  });
});
