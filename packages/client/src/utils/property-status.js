function getStatus(status) {
  switch (status) {
    case 'managed':
      return '✓';
    case 'outside':
      return '×';
    default:
    case 'unknown':
      return '?';
  }
}

module.exports = getStatus;
