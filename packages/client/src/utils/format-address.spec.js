const formatAddress = require('./format-address');

describe('formatAddress()', () => {
  it('should join parts with comma', () => {
    const address = {
      line1: 'A',
      line2: 'B',
      line3: 'C',
      line4: 'D',
      postCode: 'E',
      city: 'F',
      country: 'G'
    };
    expect(formatAddress(address)).to.equal('A, B, C, D, E, F, G');
  });

  it('should skip missing parts', () => {
    const address = {
      line1: 'A',
      line4: 'D',
      postCode: 'E',
      city: 'F'
    };
    expect(formatAddress(address)).to.equal('A, D, E, F');
  });
});
