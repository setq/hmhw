const getStatus = require('./property-status');

describe('propertyStatus()', () => {
  it('should show ✓ if property is "managed"', () => {
    expect(getStatus('managed')).to.equal('✓');
  });

  it('should show ? if status is "unknown"', () => {
    [undefined, null, 'unknown'].forEach(status => {
      expect(getStatus(status)).to.equal('?');
    });
  });

  it('should show × if status is "outside"', () => {
    expect(getStatus('outside')).to.equal('×');
  });
});
