const formatIncome = require('./format-income');

describe('formatIncome()', () => {
  it('should round amount to 2 decimal places', () => {
    expect(formatIncome(Math.PI)).to.equal('3.14');
  });

  it('should add zeroes if amount is whole number', () => {
    expect(formatIncome(10)).to.equal('10.00');
  });

  it('should return empty string if input is falsy non-number value', () => {
    [false, undefined, null, ''].forEach(input => {
      expect(formatIncome(input)).to.equal('');
    });
  });

  it('should return empty string if input is NaN or Infinity', () => {
    [NaN, Infinity, -Infinity].forEach(input => {
      expect(formatIncome(input)).to.equal('');
    });
  });

  it('should return "0.00" if amount is 0', () => {
    expect(formatIncome(0)).to.equal('0.00');
  });
});
