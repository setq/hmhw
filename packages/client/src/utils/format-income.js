function formatIncome(amount) {
  return typeof amount === 'number' && Number.isFinite(amount) ?
    amount.toFixed(2) :
    '';
}

module.exports = formatIncome;
