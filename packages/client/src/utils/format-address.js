function formatAddress({line1, line2, line3, line4, postCode, city, country}) {
  return [line1, line2, line3, line4, postCode, city, country]
    .filter(x => x)
    .join(', ');
}

module.exports = formatAddress;
