require('babel-polyfill');

const React = require('react');
const ReactDOM = require('react-dom');
const apiClient = require('hmhw-server-client');

const googleFactory = require('./services/google');
const mapsFactory = require('./services/maps');
const markersFactory = require('./services/markers');

const Property = require('./components/Property');
const ListingFactory = require('./components/Listing');
const AppFactory = require('./components/App');
const MapFactory = require('./components/Map');

window.initMap = () => {
  const G = window.google;
  const markers = markersFactory(G);
  const App = AppFactory(
    apiClient(window.fetch, '/api'),
    MapFactory(
      mapsFactory(googleFactory(G), markers),
      markers
    ),
    ListingFactory(Property)
  );
  ReactDOM.render(<App/>, document.querySelector('#app'));
};
