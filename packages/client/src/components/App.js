const {merge} = require('lodash/fp');
const React = require('react');

const styles = require('./App.css');

function AppFactory (api, Map, Listing) {
  return class App extends React.PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        isLoading: true,
        properties: [],
        focusedPropertyId: null,
        statuses: {}
      };
      this.onPropertyStatus = this.onPropertyStatus.bind(this);
      this.onPropertyFocus = this.onPropertyFocus.bind(this);
      this.onPropertyBlur = this.onPropertyBlur.bind(this);
    }

    async componentDidMount() {
      const properties = await api.getProperties();
      this.setState({isLoading: false, properties});
    }

    onPropertyStatus(id, status) {
      this.setState(({statuses}) => ({
        statuses: merge(statuses, {[id]: status})
      }));
    }

    onPropertyFocus(id) {
      this.setState({focusedPropertyId: id});
    }

    onPropertyBlur() {
      this.setState({focusedPropertyId: null});
    }

    renderListing() {
      const {properties, statuses} = this.state;
      return (
        <div className={styles.listing}>
          <Listing
            properties={properties}
            statuses={statuses}
            onPropertyMouseEnter={this.onPropertyFocus}
            onPropertyMouseLeave={this.onPropertyBlur}
          />
        </div>
      );
    }

    renderMap() {
      const {properties, focusedPropertyId} = this.state;
      return (
        <Map
          focusedPropertyId={focusedPropertyId}
          properties={properties}
          onPropertyStatus={this.onPropertyStatus}
        />
      );
    }

    renderLoader() {
      return (
        <div id="loader">
          Loading...
        </div>
      );
    }

    render() {
      const {isLoading} = this.state;
      return (
        <div className={styles.wrapper}>
          {isLoading ? this.renderLoader() : null}
          {!isLoading ? this.renderListing() : null}
          {!isLoading ? this.renderMap() : null}
        </div>
      );
    }
  };
}

module.exports = AppFactory;
