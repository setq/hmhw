const driver = require('./Property.driver');

describe('Property', () => {
  it('should render owner name', () => {
    const property = driver({owner: 'Donatas'});
    expect(property.getOwnerName()).to.equal('Donatas');
  });

  describe('Income', () => {
    it('should be rendered with currency sign', () => {
      const property = driver({incomeGenerated: 100});
      expect(property.getIncome()).to.equal('100.00£');
    });

    it('should show N/A if not available', () => {
      const property = driver({incomeGenerated: null});
      expect(property.getIncome()).to.equal('N/A');
    });
  });

  describe('Address', () => {
    it('should be rendered', () => {
      const address = {
        line1: 'A',
        line2: 'B',
        line3: 'C',
        line4: 'D',
        postCode: 'E',
        city: 'F',
        country: 'G'
      };
      const property = driver({address});
      expect(property.getAddress()).to.equal(
        'A, B, C, D, E, F, G'
      );
    });

    it('should skip missing fields', () => {
      const address = {
        line1: 'A',
        line3: 'C',
        line4: 'D',
        city: 'F',
        country: 'G'
      };
      const property = driver({address});
      expect(property.getAddress()).to.equal(
        'A, C, D, F, G'
      );
    });
  });

  it('should render status', () => {
    expect(driver({status: 'managed'}).getStatus()).to.equal('✓');
  });

  it('should invoke `onMouseEnter` callback when `onMouseEnter` is invoked on wrapper', () => {
    const onMouseEnter = sinon.spy();
    driver({onMouseEnter}).mouseEnter();
    expect(onMouseEnter).to.be.called();
  });

  it('should invoke `onMouseLeave` callback when `onMouseLeave` is invoked on wrapper', () => {
    const onMouseLeave = sinon.spy();
    driver({onMouseLeave}).mouseLeave();
    expect(onMouseLeave).to.be.called();
  });
});
