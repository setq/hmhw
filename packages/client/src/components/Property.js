const React = require('react');
const PropTypes = require('prop-types');

const formatIncome = require('../utils/format-income');
const getPropertyStatus = require('../utils/property-status');

class Property extends React.PureComponent {
  constructor(props) {
    super(props);
    this.renderAddress = this.renderAddress.bind(this);
  }

  renderAddress({line1, line2, line3, line4, postCode, city, country}) {
    const fields = [line1, line2, line3, line4, postCode, city, country];
    return (
      <td className="address">
        {fields.filter((x) => x).map((part, i) => <div key={i}>{part}</div>)}
      </td>
    );
  }

  render() {
    const {
      airbnbId: id,
      owner,
      address,
      incomeGenerated,
      status,
      onMouseEnter,
      onMouseLeave
    } = this.props;
    const income = formatIncome(incomeGenerated);
    return (
      <tr
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        className="property"
        id={'property-' + id}
      >
        <td className="owner">{owner}</td>
        {address ? this.renderAddress(address) : null}
        <td className="income">{income ? income + '£' : 'N/A'}</td>
        <td className="status">{getPropertyStatus(status)}</td>
      </tr>
    );
  }
}

Property.propTypes = {
  airbnbId: PropTypes.number,
  owner: PropTypes.string,
  address: PropTypes.shape({
    line1: PropTypes.string,
    line2: PropTypes.string,
    line3: PropTypes.string,
    line4: PropTypes.string,
    postCode: PropTypes.string,
    city: PropTypes.string,
    country: PropTypes.string
  }),
  incomeGenerated: PropTypes.number,
  status: PropTypes.oneOf(['managed', 'outside', 'unknown']),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func
};

module.exports = Property;
