const React = require('react');
const PropTypes = require('prop-types');

const styles = require('./Listing.css');

function ListingFactory(PropertyRow) {
  class Listing extends React.PureComponent {
    renderRows() {
      const {
        properties,
        statuses,
        onPropertyMouseEnter: onEnter,
        onPropertyMouseLeave: onLeave
      } = this.props;

      return properties.map((property) => {
        const id = property.airbnbId;
        return (
          <PropertyRow
            key={id}
            onMouseEnter={() => onEnter(id)}
            onMouseLeave={() => onLeave(id)}
            status={statuses[id]}
            {...property}
          />
        );
      });
    }

    render() {
      return (
        <div id="listing" className={styles.wrapper}>
          <table className={styles.listing}>
            <thead>
              <tr>
                <th>Owner</th>
                <th>Address</th>
                <th>Income Generated</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {this.renderRows()}
            </tbody>
          </table>
        </div>
      );
    }
  }

  Listing.defaultProps = {
    properties: [],
    statuses: {}
  };

  Listing.propTypes = {
    properties: PropTypes.array.isRequired,
    statuses: PropTypes.object,
    onPropertyMouseEnter: PropTypes.func,
    onPropertyMouseLeave: PropTypes.func
  };

  return Listing;
}

module.exports = ListingFactory;
