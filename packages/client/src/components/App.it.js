const apiClient = require('hmhw-server-client');

const googleFactory = require('../services/google');
const markersFactory = require('../services/markers');
const mapsFactory = require('../services/maps');
const AppFactory = require('./App');
const MapFactory = require('./Map');
const ListingFactory = require('./Listing');
const Property = require('./Property');

const listingDriver = require('./Listing.driver');

describe('App', () => {
  let api;
  let maps;
  let markers;
  let app;
  let properties;
  let appDriver;

  beforeEach(async () => {
    properties = [{airbnbId: 123}, {airbnbId: 124}];

    markers = sinon.stub(markersFactory(GOOGLE_MOCK));

    maps = sinon.stub(mapsFactory(googleFactory(GOOGLE_MOCK), markers));
    maps.getStatus.returns(Promise.resolve('managed'));

    api = sinon.stub(apiClient());
    api.getProperties.returns(Promise.resolve(properties));

    appDriver = function appDriver(props) {
      const App = AppFactory(
        api,
        MapFactory(maps, markers),
        ListingFactory(Property)
      );
      const app = createRenderer(App, props);

      const toJSON = () => app.toJSON();

      return {
        getPropertiesCount() {
          return this.getListing().getProperties().length;
        },
        getProperty(id) {
          return this.getListing().getProperty(id);
        },
        getListing() {
          return listingDriver().fromJSON(findById('listing', toJSON()));
        },
        render() {
          toJSON();
          return this;
        }
      };
    };

    app = appDriver().render();
    await nextCycle();
    app.render();
  });

  it('should render list of fetched properties', () => {
    expect(app.getPropertiesCount()).to.equal(2);
    expect(app.getProperty(123)).to.be.an('object');
    expect(app.getProperty(124)).to.be.an('object');
  });

  it('should render resolved property status', async () => {
    await nextCycle();
    expect(app.getProperty(123).getStatus()).to.equal('✓');
  });

  it('should animate property\'s marker when hovering', () => {
    app.getProperty(123).mouseEnter();
    expect(markers.animate).to.be.calledWith(123);
  });

  it('should stop animating property\'s marker on mouse leave', () => {
    app.getProperty(123).mouseEnter().mouseLeave();
    expect(markers.unanimate).to.be.calledWith(123);
  });
});

