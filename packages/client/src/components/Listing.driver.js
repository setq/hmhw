const Property = require('./Property');
const propertyDriver = require('./Property.driver');
const ListingFactory = require('./Listing');

function listingDriver(props) {
  const listing = createRenderer(ListingFactory(Property), props);

  let toJSON = () => listing.toJSON();

  return {
    getProperties() {
      return filterByClass('property', toJSON())
        .map((json) => propertyDriver().fromJSON(json));
    },
    getProperty(id) {
      return propertyDriver().fromJSON(findById('property-' + id, toJSON()));
    },
    fromJSON(json) {
      toJSON = () => json;
      return this;
    }
  };
}

module.exports = listingDriver;
