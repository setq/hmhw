const React = require('react');
const PropTypes = require('prop-types');

const styles = require('./Map.css');

function MapFactory(maps, markers) {
  class Map extends React.PureComponent {
    constructor(props) {
      super(props);
      this.setRef = this.setRef.bind(this);
    }

    componentDidMount() {
      maps.setMapElement(this.el);
      const {properties, onPropertyStatus} = this.props;
      properties.forEach(async (property) => {
        const status = await maps.getStatus(property);
        onPropertyStatus(property.airbnbId, status);
      });
    }

    componentDidUpdate(oldProps) {
      const {focusedPropertyId} = this.props;
      if (focusedPropertyId) {
        markers.animate(focusedPropertyId);
      }
      if (oldProps.focusedPropertyId) {
        markers.unanimate(oldProps.focusedPropertyId);
      }
    }

    setRef(el) {
      this.el = el;
    }

    render() {
      return  (
        <div ref={this.setRef} className={styles.wrapper} id="map">
          Loading...
        </div>
      );
    }
  }

  Map.defaultProps = {
    properties: []
  };

  Map.propTypes = {
    properties: PropTypes.array,
    onPropertyStatus: PropTypes.func,
    focusedPropertyId: PropTypes.any
  };

  return Map;
}

module.exports = MapFactory;
