const listingDriver = require('./Listing.driver');

describe('Listing', () => {
  let properties;

  beforeEach(() => {
    properties = [{
      airbnbId: 99,
      owner: 'donatas',
      address: {
        line1: '4',
        line2: 'Tower Mansions',
        line3: 'Off Station road',
        line4: '86-87 Grange Road',
        postCode: 'SE1 3BW',
        city: 'London',
        country: 'U.K.'
      },
      incomeGenerated: 1.618
    }];
  });

  it('should render a list of properties', () => {
    const properties = [
      {airbnbId: Math.random()},
      {airbnbId: Math.random()},
      {airbnbId: Math.random()}
    ];
    const listing = listingDriver({properties});
    expect(listing.getProperties().length).to.equal(3);
  });

  it('should render property owner, address, and income', () => {
    const property = listingDriver({properties}).getProperty(99);

    expect(property.getOwnerName()).to.equal('donatas');
    expect(property.getAddress()).to.equal(
      '4, Tower Mansions, Off Station road, 86-87 Grange Road,' +
        ' SE1 3BW, London, U.K.'
    );
    expect(property.getIncome()).to.equal('1.62£');
  });

  it('should pass status to a property', () => {
    const property = {airbnbId: Math.random()};
    const statuses = {[property.airbnbId]: 'managed'};
    const listing = listingDriver({properties: [property], statuses});
    expect(listing.getProperties()[0].getStatus()).to.equal('✓');
  });

  describe('Mouse Hover', () => {
    let property;
    let onPropertyMouseEnter;
    let onPropertyMouseLeave;
    let id;

    beforeEach(() => {
      onPropertyMouseEnter = sinon.spy();
      onPropertyMouseLeave = sinon.spy();
      id = Math.random();
      const listing = listingDriver({
        onPropertyMouseEnter,
        onPropertyMouseLeave,
        properties: [{airbnbId: id}]
      });
      property = listing.getProperties()[0];
    });

    it('should invoke `onPropertyMouseEnter` when mouse enters property row', () => {
      property.mouseEnter();
      expect(onPropertyMouseEnter).to.be.calledWith(id);
    });

    it('should invoke `onPropertyMouseLeave` when mouse leaves property row', () => {
      property.mouseLeave();
      expect(onPropertyMouseLeave).to.be.calledWith(id);
    });
  });
});
