const React = require('react');
const apiClient = require('hmhw-server-client');

const AppFactory = require('./App');

describe('<App>', () => {
  let renderer, json;
  let api;
  let Map;
  let Listing;
  let App;

  beforeEach(() => {
    api = sinon.stub(apiClient());
    Map = function Map(props) {
      return React.createElement('Map', props);
    };
    Listing = function Listing(props) {
      return React.createElement('Listing', props);
    };
    App = AppFactory(api, Map, Listing);
  });

  describe('when loading property list', () => {
    beforeEach(() => {
      api.getProperties.returns(new Promise(() => {}));
      renderer = createRenderer(App);
      json = renderer.toJSON();
    });

    it('should not render listing element', () => {
      expect(findByType('Listing', json)).to.be.undefined();
    });

    it('should not render map element', () => {
      expect(findByType('Map', json)).to.be.undefined();
    });

    it('should render loader', () => {
      expect(findById('loader', json)).to.be.an('object');
    });
  });

  describe('when property list loads', () => {
    let properties;

    beforeEach(async () => {
      properties = [{airbnbId: 123}, {airbnbId: 124}];
      api.getProperties.returns(Promise.resolve(properties));
      renderer = createRenderer(App);
      render();
      await nextCycle();
      render();
    });

    function render() {
      json = renderer.toJSON();
    }

    it('should render listing element', () => {
      expect(findByType('Listing', json)).to.be.an('object');
    });

    it('should pass properties to listing', () => {
      const listing = findByType('Listing', json);
      expect(listing.props.properties).to.deep.equal(properties);
    });

    it('should initially pass empty statuses object to listing', () => {
      const listing = findByType('Listing', json);
      expect(listing.props.statuses).to.deep.equal({});
    });

    it('should render map element', () => {
      expect(findByType('Map', json)).to.be.an('object');
    });

    it('should pass map element a list of properties', () => {
      const map = findByType('Map', json);
      expect(map.props.properties).to.deep.equal(properties);
    });

    it('should pass updated property status to listing when map reports it', async () => {
      const map = findByType('Map', json);
      map.props.onPropertyStatus(123, 'managed');
      render();
      const listing = findByType('Listing', json);
      expect(listing.props.statuses).to.deep.equal({123: 'managed'});
    });

    it('should have no focused property by default', () => {
      expect(findByType('Map', json).props.focusedPropertyId).to.be.null();
    });

    it('should tell map focused property', () => {
      findByType('Listing', json).props.onPropertyMouseEnter(1);
      render();
      expect(findByType('Map', json).props.focusedPropertyId).to.equal(1);
    });

    it('should remove property focus', () => {
      findByType('Listing', json).props.onPropertyMouseEnter(1);
      findByType('Listing', json).props.onPropertyMouseLeave(1);
      render();
      expect(findByType('Map', json).props.focusedPropertyId).to.be.null();
    });
  });
});
