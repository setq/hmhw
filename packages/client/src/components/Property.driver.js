const Property = require('./Property');

function propertyDriver(props) {
  const property = createRenderer(Property, props);

  let toJSON = () => property.toJSON();

  return {
    mouseEnter() {
      toJSON().props.onMouseEnter();
      return this;
    },
    mouseLeave() {
      toJSON().props.onMouseLeave();
      return this;
    },
    getOwnerName() {
      return findByClass('owner', toJSON()).children[0];
    },
    getIncome() {
      return findByClass('income', toJSON()).children[0];
    },
    getAddress() {
      return findByClass('address', toJSON())
        .children.map((node) => node.children[0])
        .join(', ');
    },
    getStatus() {
      return findByClass('status', toJSON())
        .children[0];
    },
    fromJSON(json) {
      toJSON = () => json;
      return this;
    }
  };
}

module.exports = propertyDriver;
