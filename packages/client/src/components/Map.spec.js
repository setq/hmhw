const React = require('react');

const googleFactory = require('../services/google');
const mapsFactory = require('../services/maps');
const markersFactory = require('../services/markers');
const MapFactory = require('./Map');

describe('Map', () => {
  let google;
  let markers;
  let maps;
  let Map;
  let Wrapper;

  beforeEach(() => {
    google = sinon.stub(googleFactory(GOOGLE_MOCK));
    markers = sinon.stub(markersFactory());
    maps = sinon.stub(mapsFactory(google, markers));
    maps.getStatus.returns(new Promise(() => {}));
    Map = MapFactory(maps, markers);

    Wrapper = class Wrapper extends React.PureComponent {
      constructor(props) {
        super(props);
        this.state = props;
      }

      render() {
        return (
          <div updateProps={(props) => this.setState(props)}>
            <Map {...this.state}/>
          </div>
        );
      }
    };
  });

  it('should create google map using underlying element', () => {
    const el = {id: Math.random()};
    renderMap({}, {createNodeMock: () => el});
    expect(maps.setMapElement).to.be.calledWith(el);
  });

  it('should get status of property passed', () => {
    const property = {airbnbId: Math.random()};
    const props = {properties: [property]};
    renderMap(props);
    expect(maps.getStatus).to.be.calledWith(property);
  });

  it('should report property status via `onPropertyStatus` callback', async () => {
    const id = Math.random();
    const status = Math.random();
    const onPropertyStatus = sinon.stub();
    maps.getStatus.returns(Promise.resolve(status));

    renderMap({
      onPropertyStatus,
      properties: [{airbnbId: id}]
    });
    await nextCycle();

    expect(onPropertyStatus).to.be.calledWith(id, status);
  });

  describe('Marker animation', () => {
    let focusedPropertyId;
    let renderer;
    let json;

    beforeEach(() => {
      focusedPropertyId = Math.random();
      renderer = createRenderer(Wrapper);
      json = renderer.toJSON();
    });

    it('should animate marker when `focusedPropertyId` is updated', () => {
      json.props.updateProps({focusedPropertyId});
      expect(markers.animate).to.be.calledWith(focusedPropertyId);
    });

    it('should stop animating marker when `focusedPropertyId` is updated to be falsy', () => {
      json.props.updateProps({focusedPropertyId});
      json.props.updateProps({focusedPropertyId: null});
      expect(markers.unanimate).to.be.calledWith(focusedPropertyId);
    });

    it('should switch animating markers when `focusedPropertyId` is updated to new value', () => {
      const newId = Math.random();
      json.props.updateProps({focusedPropertyId});
      json.props.updateProps({focusedPropertyId: newId});
      expect(markers.unanimate).to.be.calledWith(focusedPropertyId);
      expect(markers.animate).to.be.calledWith(newId);
    });
  });


  function renderMap(props, options = {}) {
    return createRenderer(Map, props, options).toJSON();
  }
});
