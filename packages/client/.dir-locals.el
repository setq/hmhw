((nil . ((eval . (progn
                   (setq-local flycheck-javascript-eslint-executable
                               (expand-file-name "packages/client/node_modules/.bin/eslint"
                                                 (projectile-project-root))))))))
