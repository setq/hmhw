#!/user/bin/env bash

set -x

if [ "$1" = "-w" ]; then
    COMMAND="watchify -v"
else
    COMMAND=browserify
fi

export NODE_ENV=production

$COMMAND \
    -p [ css-modulesify -o public/bundle.css ] \
    -t babelify \
    -e src/index.js \
    -o public/bundle.js
