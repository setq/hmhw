const React = require('react');
const ReactTestRenderer = require('react-test-renderer');
const chai = require('chai');
const sinon = require('sinon');

chai.use(require('dirty-chai'));
chai.use(require('sinon-chai'));

function tree_fold(node, acc, f) {
  return list_fold(
    Array.isArray(node) ? node : (node && node.children || []),
    f(acc, node),
    (acc, node) => tree_fold(node, acc, f));
}

function list_fold(list, acc, f) {
  for (let i = 0; i < list.length; i++) {
    acc = f(acc, list[i]);
  }
  return acc;
}

function filterBy(p, tree) {
  tree = Array.isArray(tree) ? tree : [tree];
  return Array.prototype.concat.apply(
    [],
    tree.map(node => tree_fold(node, [], (acc, node) => p(node) ? acc.concat(node) : acc))
  );
}

function filterByType(type, tree) {
  return filterBy(node => node && node.type && node.type === type, tree);
}

function filterById(id, tree) {
  return filterBy(node => node && node.props && node.props.id === id, tree);
}

function filterByClass(klass, tree) {
  const re = new RegExp(`\\b${klass}\\b`);
  return filterBy(
    node => node && node.props && re.test(node.props.className),
    tree
  );
}

function findById(id, tree) {
  return filterById(id, tree)[0];
}

function findByType(type, tree) {
  return filterByType(type, tree)[0];
}

function findByClass(klass, tree) {
  return filterByClass(klass, tree)[0];
}

function createRenderer(Component, props, options = {}) {
  return ReactTestRenderer.create(
    <Component {...props}/>,
    options
  );
}

function nextCycle() {
  return new Promise((resolve) => setTimeout(resolve));
}

const GOOGLE_MOCK = {
  maps: {
    Animation: {
      BOUNCE: 'BOUNCE'
    },
    Geocoder: class {},
    Marker: class {},
    LatLng: class {}
  }
};

global.sinon = sinon;
global.expect = chai.expect;
global.filterBy = filterBy;
global.filterByType = filterByType;
global.filterById = filterById;
global.filterByClass = filterByClass;
global.findByType = findByType;
global.findById = findById;
global.findByClass = findByClass;
global.createRenderer = createRenderer;
global.nextCycle = nextCycle;
global.GOOGLE_MOCK = GOOGLE_MOCK;
