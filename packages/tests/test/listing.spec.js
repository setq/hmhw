const {spawn} = require('child_process');
const path = require('path');
const pup = require('puppeteer');

describe('Listing', () => {
  let server;
  let browser;
  let page;

  before(async () => {
    server = await runServer(path.join('..', 'server', 'bin.js'), [
      '-p', 8080,
      '-d', path.join('..', 'client', 'public')
    ]);
    browser = await pup.launch();
    page = await browser.newPage();
    await page.goto('http://localhost:8080/');
  });

  after(async () => {
    await browser.close();
    server.kill();
  });

  it('shows a list of properties', async () => {
    const table = await page.evaluate(
      (listing) => {
        const body = listing.querySelector('tbody');
        const rows = Array.from(body.querySelectorAll('tr'));
        return rows.map((tr) => {
          const [owner, _address, income, status] = Array.from(tr.querySelectorAll('td'))
            .map((td) => td.innerText);
          const address = _address.trim().split('\n').join(', ');
          return [owner, address, income, status];
        });
      },
      await page.waitForSelector('#listing', {visible: true})
    );
    expect(table).to.deep.equal([
      ['somebody', 'Longford TW6, London, UK', '123456789.00£', '×'],
      ['carlos', 'Flat 5, 7 Westbourne Terrace, W2 3UL, London, U.K.', '2000.34£', '✓'],
      [
        'ankur',
        '4, Tower Mansions, Off Station road, 86-87 Grange Road, SE1 3BW, London, U.K.',
        '10000.00£',
        '✓'
      ],
      ['elaine', '4, 332b, Goswell Road, EC1V 7LQ, London, U.K.', '1200.00£', '✓']
    ]);
  });

  it('displays a map', async () => {
    const map = await page.waitForSelector('#map', {visible: true});
    const populated = await page.evaluate((el) => !!el.children.length, map);
    expect(populated).to.equal(true);
  });
});

function runServer(command, args) {
  const options = {stdio: 'pipe'};
  return new Promise((resolve, reject) => {
    const handle = spawn(command, args, options);
    handle.on('error', reject);
    let output = '';
    handle.stdout.on('data', (chunk) => {
      output += chunk.toString();
      if (/server started on port 8080/.test(output)) {
        resolve(handle);
      }
    });
  });
}
