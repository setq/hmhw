function serverClient(fetch, baseUrl) {
  return {
    getProperties() {
      return fetch(baseUrl + '/properties')
        .then(response => response.json());
    }
  };
}

module.exports = serverClient;
