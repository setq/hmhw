const fetch = require('node-fetch');
const serverClient = require('hmhw-server-client');

const startServer = require('./index');

describe('Server', () => {
  let server;
  let client;
  const PORT = 9002;

  beforeEach(async () => {
    server = await startServer({port: PORT, statics: '.'});
  });

  afterEach(() => {
    server.stop();
  });

  it('serves statics directory', async () => {
    const response = await fetch('http://localhost:9002/package.json');
    expect(await response.json()).to.deep.equal(require('../package.json'));
  });

  describe('API', () => {
    let client;

    beforeEach(() => client = serverClient(fetch, 'http://localhost:9002/api'));

    it('returns a list of predefined properties', async () => {
      const properties = await client.getProperties();
      expect(properties).to.deep.equal(require('./properties.json'));
    });
  });
});
