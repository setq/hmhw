const express = require('express');

const properties = require('./properties.json');

function startServer({port, statics}) {
  const app = express();

  if (statics) {
    app.use(express.static(statics));
  }

  app.get('/api/properties', (req, res) => {
    res.send(properties);
  });

  return new Promise((resolve) => {
    const server = app.listen(port, () => {
      console.log(`server started on port ${port} serving ${statics}`);
      resolve({
        stop: () => server.close()
      });
    });
  });
}

module.exports = startServer;
