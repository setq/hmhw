#!/usr/bin/env node

const program = require('commander');
const startServer = require('./src/index');

program
  .option('-d, --directory <DIR>', 'Directory to serve.')
  .option('-p, --port [PORT]', 'Port to serve on.', 8080)
  .parse(process.argv);

startServer({port: program.port, statics: program.directory});
